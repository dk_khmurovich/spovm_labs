#include <windows.h>
#include <iostream>

#define MAX_SEM_COUNT 1
#define SEMAPHORE_NAME "sem"
#define PIPE_NAME "\\\\.\\pipe\\socket"

HANDLE hSemaphore;

int main(int argc, char* argv[]){

    LPSTR semName = SEMAPHORE_NAME;
    hSemaphore = CreateSemaphore(NULL, MAX_SEM_COUNT, MAX_SEM_COUNT, semName);
    if(!hSemaphore){
        std::cout << "Create semaphore error : " << GetLastError() << std::endl;
    }

    LPSTR pipeName = PIPE_NAME;

    DWORD byteRecived, byteSended;

    char reciveBuffer[1024];

    HANDLE hPipe = CreateNamedPipe(pipeName, PIPE_ACCESS_DUPLEX, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
    PIPE_UNLIMITED_INSTANCES, 1024, 1024, 1000, NULL);

    if(hPipe == INVALID_HANDLE_VALUE){
        std::cout << "Cannot create pipe, error : " << GetLastError() << std::endl;
        return 0;
    }

    BOOL pipeConnection;

    std::cout << "Waiting for connection..." << std::endl;

    pipeConnection = ConnectNamedPipe(hPipe, NULL);

    std::cout << "Client connected" << std::endl << "Waiting for command..." << std::endl;

    if(!pipeConnection){
        std::cout << "Something wrong, can't connect to pipe" << std::endl;
        CloseHandle(hSemaphore);
        CloseHandle(hPipe);
        return 0;
    }

    while(true){
        if(ReadFile(hPipe, reciveBuffer, 1024, &byteRecived, NULL)){
            reciveBuffer[byteRecived] = 0;
            printf("Recived : <%s>\n", reciveBuffer);
            if(!WriteFile(hPipe, reciveBuffer, byteRecived, &byteSended, NULL)){
                break;
            }
            if(!strcmp(reciveBuffer, "exit")){
                break;
            }
        }
        else{
            std::cout << "Read pipe error : " << GetLastError() << std::endl;
            break;
        }
    }

    CloseHandle(hSemaphore);
    CloseHandle(hPipe);
    return 0;
}