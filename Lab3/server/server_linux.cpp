#include <iostream>
#include <semaphore.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <errno.h>

#define SEMAPHORE_NAME "sem5"
#define BUFFER_SIZE 1024

sem_t* semaphore;
int _pipe[2];

void client_process(){
    char buffer[BUFFER_SIZE];
    int byteRecived = 0, byteSended = 0;
    int value = 0;
    std::cout << "clinet>Wainting connection..." << std::endl;
    std::cout << "clinet>Connected, type 'exit' to terminate" << std::endl;
    while(true){
        sem_wait(semaphore);
        //std::cout << "Semaphore captured!" << std::endl;
        std::cin.getline(buffer, BUFFER_SIZE);
        //printf("%s\n with length %ld", buffer, strlen(buffer));
        byteSended = write(_pipe[1], buffer, strlen(buffer));
        //printf("Byte sended %d\n", byteSended);
        if(byteSended < 0){
            std::cout << "clinet>Cannot send message to server" << std::endl;
            break;
        }
        sem_post(semaphore);
        do{
            sem_getvalue(semaphore, &value);
            //std::cout << "Hi" << std::endl;
        }while(value);
        sem_wait(semaphore);
        byteRecived = read(_pipe[0], buffer, BUFFER_SIZE);
        //printf("Byte recived %d\n", byteRecived);
        if(byteRecived >= 0){
            buffer[byteRecived] = 0;
            printf("client>Server : <%s>\n", buffer);
        }
        else{
            std::cout << "Cannot read from pipe" << std::endl;
            break;
        }
        if(!strcmp(buffer, "exit"))
            break;
        sem_post(semaphore);
    }

    sem_post(semaphore);
    return;    
}

int main(){
    char buffer[BUFFER_SIZE];
    int byteRecived = 0, byteSended = 0;
    semaphore = sem_open(SEMAPHORE_NAME, O_CREAT, S_IRWXU, 1);
    int value = 0;
    sem_getvalue(semaphore, &value);
    //printf("Semaphore state %d\n", value);
    if(semaphore == SEM_FAILED){
        std::cout << "server>Cannot create semaphore" << std::endl;
        return 0;
    }

    if(pipe(_pipe)<0){
        std::cout << "Cannot create pipe" << std::endl;
        return 0;
    }


    int pid = fork();

    switch(pid){
        case -1:
            std::cout << "server>Cannot create child process" << std::endl;
            return 0;
        case 0:
            client_process();
            std::cout << "Child process was ended!" << std::endl;
            return 0;
        default:
            break;
    }
    std::cout << "server>Server ready!" << std::endl;
    std::cout << "server>Waiting for connection..." << std::endl;

    while(value){
        sem_getvalue(semaphore, &value);
    }

    std::cout << "server>Connected!" << std::endl;
    while(true){
        sem_wait(semaphore);
        //std::cout << "I'm here!" << std::endl;
        byteRecived = read(_pipe[0], buffer, BUFFER_SIZE);
        if(byteRecived >= 0){
            buffer[byteRecived] = 0;
            printf("server>Client : <%s>\n", buffer);
            byteSended = write(_pipe[1], buffer, byteRecived); 
            if(byteSended < 0){
                std::cout << "server>Cannot write to pipe" << std::endl;
                break;
            }
            if(!strcmp(buffer, "exit")){
                sem_post(semaphore);
                do{
                    sem_getvalue(semaphore, &value);
                }while(value);
                break;
            }
            }
        else{
            std::cout << "server>Cannot read from pipe" << std::endl;
            break;
        }
        sem_post(semaphore);
        do{
            sem_getvalue(semaphore, &value);
        }while(value);
    }

    sem_wait(semaphore);
    sem_destroy(semaphore);
    close(_pipe[0]);
    close(_pipe[1]);
    std::cout << "Parent process was ended!" << std::endl;
    return 0;
}