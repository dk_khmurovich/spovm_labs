#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <conio.h>

#define PIPE_NAME "\\\\.\\pipe\\socket"
#define SEMAPHORE_NAME "sem"

int main(){
    DWORD byteSended, byteRecived;
    char recivedBuffer[1024];
    HANDLE hPipe;
    HANDLE hSemaphore;
    LPSTR pipeName = PIPE_NAME;
    LPSTR semName = SEMAPHORE_NAME;

    hSemaphore = OpenSemaphore(SEMAPHORE_ALL_ACCESS, FALSE, semName);
    if(!hSemaphore){
        std::cout << "Cannot open semaphore, error : " << GetLastError() << std::endl;
        return 0;
    }

    if(WaitForSingleObject(hSemaphore, 5000) != WAIT_OBJECT_0){
        std::cout << "Cannot connect to server, try again later" << std::endl;
        CloseHandle(hSemaphore);
        return 0;
    }

    hPipe = CreateFile(pipeName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if(hPipe == INVALID_HANDLE_VALUE){
        std::cout << "Cannot open pipe, error : " << GetLastError() << std::endl;
        ReleaseSemaphore(hSemaphore, 1, NULL);
        CloseHandle(hSemaphore);
        return 0;
    }

    std::cout << "Connectd. Type 'exit' to terminate" << std::endl;

    while(true){
        std::cout << "client>";
        gets_s(recivedBuffer, 1024);
        if(!WriteFile(hPipe, recivedBuffer, strlen(recivedBuffer), &byteSended, NULL)){
            std::cout << "Cannot send message to server, error : " << GetLastError() << std::endl;
            break; 
        }
        if(ReadFile(hPipe, recivedBuffer, 1024, &byteRecived, NULL)){
            recivedBuffer[byteRecived] = 0;
            printf("Server : <%s>\n", recivedBuffer);
        }
        else{
            std::cout << "Cannot recive message from server, error : " << GetLastError() << std::endl;
            break;
        }
        if(!strcmp(recivedBuffer, "exit"))
            break; 
    }
    ReleaseSemaphore(hSemaphore, 1, NULL);
    CloseHandle(hSemaphore);
    CloseHandle(hPipe);
    return 0;
}