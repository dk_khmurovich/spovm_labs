#include <iostream>
#include <Windows.h>

int main(int argc, char** argv)
{
  int pid = 0;
  pid = GetCurrentProcessId();
  char string_to_out[30] = {0};
  sprintf(string_to_out, "hello%d", pid);
  char eventName[30] = {0};
  sprintf(eventName, "ChildEventStart%d", pid);
  HANDLE hEventStart = OpenEventA(EVENT_ALL_ACCESS, TRUE, eventName);
  sprintf(eventName, "ChildEventEnd%d", pid);
  HANDLE hEventEnd = OpenEventA(EVENT_ALL_ACCESS, TRUE, "END_WRITE");
  HANDLE eventArr[] = {hEventStart, hEventEnd};
  while(1){
    if(WaitForMultipleObjects(2, eventArr, FALSE, INFINITY) == WAIT_OBJECT_0){
      ResetEvent(hEventStart);
      int counter = 0;
      while(string_to_out[counter]){
          Sleep(250);
          printf("%c", string_to_out[counter]);
          ++counter;
      }
      printf("%d\n", pid);
      SetEvent(hEventEnd);
    }
  }
  return 0;
}