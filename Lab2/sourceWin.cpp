#include <iostream>
#include <Windows.h>
#include <conio.h>
#include <vector>


HANDLE createProcess(char* argument){
    STARTUPINFO info;
    ZeroMemory(&info, sizeof(STARTUPINFO));
    info.cb = sizeof(info);
    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
    if(CreateProcess("D:\\Documents\\Projects\\SP\\Lab2\\childProcess\\build\\Debug\\childProcess.exe", argument, NULL, NULL, TRUE, 0, NULL, NULL, &info, &pi) == TRUE){
        return pi.hProcess;   
    }
    printf("Process not created\n");
    return 0;
}

int getchBundle()
{
	if (_kbhit()) return _getch();
	else return -1;
}

int main(int argc, char* argv[]){
    HANDLE pids[101];
    HANDLE hResourceEventsStart[101];
    HANDLE hResourceEventEnd = CreateEventA(NULL, TRUE, TRUE, "END_WRITE");
    int pidNumber = 0;
    int currentProccess = pidNumber;
    int prevProcess = 0;
    int new_proc_flag = FALSE;
    char symbol = 0;
    int end = 0;
    char eventName[30] = {0};

    while(!end){
        symbol = getchBundle();
        //std::cout << "Loop test" << std::endl;
        switch (symbol){
        case '+':
            if(pidNumber > 101)
                break;
            pids[pidNumber] = createProcess(argv[1]);
            sprintf(eventName, "ChildEventStart%d", GetProcessId(pids[pidNumber]));
            hResourceEventsStart[pidNumber] = CreateEventA(NULL, TRUE, FALSE, eventName);
            if(pidNumber == 0)
                SetEvent(hResourceEventsStart[pidNumber]);
            ++pidNumber;
            break;
        case '-':
            if(!pidNumber)
                break;
            if(WaitForSingleObject(hResourceEventEnd, INFINITY) == WAIT_OBJECT_0){
                TerminateProcess(pids[pidNumber - 1], 0);
                CloseHandle(hResourceEventsStart[pidNumber - 1]);
                if(currentProccess == pidNumber - 1)
                    currentProccess = 0;
            }
            --pidNumber;
            break;
        case 'q':
            while (pidNumber){
			    if(WaitForSingleObject(hResourceEventEnd, INFINITY) == WAIT_OBJECT_0){
                TerminateProcess(pids[pidNumber - 1], 0);
                CloseHandle(hResourceEventsStart[pidNumber - 1]);
                --pidNumber;
		        }
            }
            CloseHandle(hResourceEventEnd);
            return 0;
        default:
            break;
        }
        if(pidNumber){
            prevProcess = (currentProccess) ? (currentProccess-1) : (pidNumber - 1); 
            if(WaitForSingleObject(hResourceEventEnd, INFINITY) == WAIT_OBJECT_0){
                SetEvent(hResourceEventsStart[currentProccess]);
                ResetEvent(hResourceEventEnd);
                ++currentProccess;
                if(currentProccess >= pidNumber){
                    currentProccess = 0;
                }
            }                
        }
    }
    //CloseHandle(hResourceEvent);
    return 0;
}

