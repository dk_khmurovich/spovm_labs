#include <signal.h>
#include <termios.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stropts.h>
#include <string>
#include <iostream>
#include <time.h>

int start_write = 0;
int end_write = 0;
int end_process = 0;

struct sigaction sig_to_end_write, sig_to_write;

void action_to_end_write(int){
    end_write = 1;
}


void action_to_write(int){
    start_write = 1;
    end_write = 0;
}

void action_to_end_child_process(int){
    end_process = 1;
}

int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;
 
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
 
  ch = getchar();
 
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);
 
  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }
 
  return 0;
}

int getch()
{
  struct termios oldt, newt;
  int ch;
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  return ch;
}

int getchBundle()
{
	if (kbhit()) return getch();
	else return -1;
}


int main(int argc, char* argv[]){
    int pids[101];
    int pidNumber = 0;
    int currentProcess = 0;
    int end = 0;
    
    sig_to_end_write.sa_handler = action_to_end_write;
    sig_to_write.sa_handler = action_to_write;
    sigaction(SIGUSR2, &sig_to_end_write, NULL);
    sigaction(SIGUSR1, &sig_to_write, NULL);


    while(!end){
        switch(getchBundle()){
            case '+':
                if(pidNumber > 101)
                    break;
                pids[pidNumber] = fork();
                ++pidNumber;
                if(pidNumber == 1 && pids[pidNumber - 1])
                    kill(pids[pidNumber - 1], SIGUSR1);
                switch (pids[pidNumber - 1]){
                    case -1:{
                        std::cerr << "Error" << std::endl;
                        break;
                    }
                    case 0:{
                        int pid = 0;
                        pid = getpid();
                        char* string_to_out = argv[1];
                        sig_to_end_write.sa_handler = action_to_end_child_process;
                        sigaction(SIGUSR2, &sig_to_end_write, NULL);
                        while(!end_process){
                            if(start_write && !end_write){
                                if(end_write) return 0;
                                printf("%d", pid);
                                fflush(stdout);
                                int counter = 0;
                                while(string_to_out[counter]){
                                    usleep(100000);
                                    printf("%c", string_to_out[counter]);
                                    fflush(stdout);
                                    ++counter;
                                }
                                start_write = 0;
                                end_write = 1;
                                std::cout << std::endl;
                                kill(getppid(), SIGUSR2);
                            }
                        }
                        return 0;
                    }
                    default:
                        break;
                }
            break;
            case '-':
                if(!pidNumber)
                    break;
                kill(pids[pidNumber - 1], SIGUSR2);
                waitpid(pids[pidNumber - 1], NULL, NULL);
                --pidNumber;
            break;
            case 'q':
                end = 1;
            break;
            default: break;
        }
        if(pidNumber && end_write){
                if(end_write){
                    //kill(pids[currentProcess], SIGUSR2);
                    ++currentProcess;
                    if(currentProcess >= pidNumber){
                        currentProcess = 0;
                        //refresh();
                    }
                    end_write = 0;
                    kill(pids[currentProcess], SIGUSR1);
                }
            }
    }
    while(pidNumber){
        kill(pids[pidNumber - 1], SIGUSR2);
        waitpid(pids[pidNumber - 1], NULL, NULL);
        --pidNumber; 
    }
    return 0;
}