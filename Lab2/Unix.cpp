#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <vector>
#include <sys/wait.h>

struct sigaction printSignal, endSignal;

int allowPrinting = 0,
    parentID = 0,
    endProcess = 1;

void canIPrintHandler(int signal)
{
    allowPrinting = 1;
}

void setEndHandler(int signal)
{   
    endProcess = 1;
}

int main()
{
    //initscr();
    //cbreak();
    initscr();
    clear();
    noecho();
    refresh();

    std::vector<pid_t> pidVector;
    char symbol;
    int currentProcess = 0;
    bool wasProcessKilled = false;

    printSignal.sa_handler = canIPrintHandler;
    endSignal.sa_handler = setEndHandler;
    sigaction(SIGUSR1, &printSignal, NULL);
    sigaction(SIGUSR2, &endSignal, NULL);

    parentID = getpid();
    //printf("%d, %d", getppid(), getpid());
    //refresh();

    while(symbol != 'q')
    {
        symbol = getchar();
        //printw(&symbol);
        switch (symbol)
        {
        case '+':
        {
            pidVector.push_back(fork());

            switch (pidVector.back())
            {
            case -1:
            {
                std::cout << "Error" << std::endl;
                break;
            }
            case 0:
            {
                endProcess = 0;
                //printf("%d () ", getppid());
                while(!endProcess)
                { 
                    usleep(100000);

                    if(allowPrinting)
                    {
                        if(endProcess) return 0;
                        printf("%d ", pidVector.size());
                        refresh();
                        usleep(100000);
                    }

                    allowPrinting = 0;
                    kill(getppid(), SIGUSR2);
                    
                }
                //printf("Ended");
                //refresh();
                return 0;
            }
            default:
            {
                break;
            }
            }

            break;
        }
        case '-':
        {
            if(!pidVector.size()) break;

            kill(pidVector.back(), SIGUSR2);
            waitpid(pidVector.back(), NULL, NULL);

            if(currentProcess >= pidVector.size())
            {
                currentProcess = 0;
                endProcess = 1;
                wasProcessKilled = true; 
            }

            pidVector.pop_back();

            break;
        }
        default:
        {
            break;
        }
        }

        //printf
        if(pidVector.size() && endProcess)
        {
            endProcess = 0;
            //printf("Here");
            if(currentProcess >= pidVector.size() - 1) 
            {
              currentProcess = 0;
              //printf("\n");
              refresh();
            }
            else if(!wasProcessKilled) currentProcess++;

            wasProcessKilled = false;
            kill(pidVector[currentProcess], SIGUSR1);
        }
    }
    endwin();
    return 0;
}