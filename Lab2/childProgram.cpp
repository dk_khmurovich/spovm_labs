#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/wait.h>

int start_write = 1;

struct sigaction sig_to_write;

void action_to_write(int){
  start_write = 1;
  printf("Set flag to write\n");
}

int childfunction(char arg)
{
  std::cout << "CHILD PROC" << std::endl;
  int id, i = 0, number, pid = 0;
  pid = getpid();
  char* string_to_out = arg;
  while(1)
  {
    if(start_write){
      start_write = 0;
      printf("%d", pid);
      int counter = 0;
      while(string_to_out[counter]){
          usleep(500000);
          printf("%c", string_to_out[counter]);
          fflush(stdout);
          ++counter;
      }
      printf("\n");
      kill(getppid(), SIGUSR2);
      kill(pid, SIGSTOP);
    }
  }
  return 0;
}