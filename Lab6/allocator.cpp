#include <iostream>
#include <unistd.h>
#include <sys/mman.h>
#include <assert.h>
#include <iomanip>
#include <string.h>

/*----------------------------------------------------------------*
*Need to write merge and split to reallocation and defragmentation*
*Probably use dual linked list model instead of common linked list*
*And need to check if it's really working, add free to allocator  *
*-----------------------------------------------------------------*/

typedef size_t word_t;

static size_t arena_size = 4194304; // 4 MB

static word_t* arena = reinterpret_cast<word_t*>(mmap(0, arena_size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0));

static word_t* _brk = arena;

static word_t* heap_start = nullptr;

static word_t* heap_top = nullptr;


struct Block{
    size_t size;
    bool used;
    struct Block* next;
    word_t data[1];
};

word_t* _sbrk(size_t increment){
    if(increment == 0){
        return _brk;
    }
    if((reinterpret_cast<char*>(_brk)+increment) > (reinterpret_cast<char*>(arena)+arena_size)){
        return reinterpret_cast<word_t*>(-1);
    }
    if(increment < 0){
        return nullptr;
    }

    _brk = reinterpret_cast<word_t*>(reinterpret_cast<char*>(_brk)+increment);

    return _brk;
}

size_t align(size_t size){
    //std::cout << "Align size : " << ((size + sizeof(word_t) - 1) & ~(sizeof(word_t) - 1)) << std::endl;
    return (size + sizeof(word_t) - 1) & ~(sizeof(word_t) - 1);
}

size_t allocSize(size_t size){
    //std::cout << "Allocated size : " << (size + sizeof(Block) - sizeof(std::declval<Block>().data)) << std::endl;
    //std::cout << "Size : " << size << std::endl;
    //std::cout << "Block size : " << sizeof(Block) << std::endl;
    //std::cout << "Min data size : " << sizeof(std::declval<Block>().data) << std::endl;
    return size + sizeof(Block) - sizeof(std::declval<Block>().data);
}

Block* mapmem(size_t size){
    auto block = reinterpret_cast<Block*>(_sbrk(0));

    if(_sbrk(allocSize(size)) == reinterpret_cast<word_t*>(-1)){
        return nullptr;
    }

    return block;
}

Block* splitBlock(Block* block, size_t rec_size){
    block->size /= 2;
    auto next_block = reinterpret_cast<Block*>(block->data + block->size);
    next_block->size = block->size;
    next_block->used = false;
    next_block->next = block->next;
    block->next = next_block;
    if(block->size > rec_size)
        return splitBlock(block, rec_size);

    return block;
}

Block* mergeBlocks(Block* block){
    block->size += block->next->size;
    block->next = block->next->next;

    return block;
}

Block* findBest(size_t size){
    auto block = reinterpret_cast<Block*>(heap_start);
    while(block != nullptr){
        if(!block->used && block->size >= size){
            /*if(block->size > size)
                return splitBlock(block, size);
            if(block->size < size);*/
            block->used = true;
            break;
        }
        block = block->next;
    }

    return block;
}

word_t* alloc(size_t size){
    auto block = findBest(size);
    if(block != nullptr){
        return block->data;
    }
    
    size = align(size);
    block = mapmem(size);

    if(block == nullptr){
        return reinterpret_cast<word_t*>(-1);
    }

    block->size = size;
    block->used = true;
    block->next = nullptr;


    if(heap_start == nullptr){
        heap_start = reinterpret_cast<word_t*>(block);
    }

    if(heap_top != nullptr){
        reinterpret_cast<Block*>(heap_top)->next = block;
    }

    heap_top = reinterpret_cast<word_t*>(block);

    return block->data;
}

Block* getHeader(word_t* ptr){
    return reinterpret_cast<Block*>(reinterpret_cast<char*>(ptr) + sizeof(std::declval<Block>().data) - sizeof(Block)); 
}

void _free(word_t* ptr){
    auto block = getHeader(ptr);
    block->used = false;
}

word_t* _realloc(word_t* ptr, size_t size){
    if(ptr == nullptr)
        return ptr;

    auto block = getHeader(ptr);

    if(block->size >= size){
        return ptr;
    }

    word_t* new_block = alloc(size);
    if(new_block != reinterpret_cast<word_t*>(-1)){
        memcpy(new_block, ptr, block->size);
        _free(ptr);
    }

    return new_block;
}

void unmapmem(){
    munmap(arena, arena_size);
}

int main(){

    char* p = nullptr;

    word_t* ptr = alloc(12);
    *ptr = 1;
    word_t* ptr1 = alloc(16);
    word_t* ptr2 = alloc(32);
    word_t* ptr3 = alloc(8);

    *ptr = 1;
    *ptr1 = 2;
    *ptr2 = 3;
    *ptr3 = 4;

    _free(ptr1);
    assert(ptr1 == alloc(4));

    unmapmem();

    return 0;
}