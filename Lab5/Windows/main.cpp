#include "reader.h"
#include "writer.h"
#include <filesystem>
#include <algorithm>


typedef struct {
    std::string buffer;
    size_t byte_received;
    size_t prev_value;
}FileInfo;

typedef struct {
    FileInfo fileinfo;
}ThreadObject;

bool exit_flag = false;


DWORD WINAPI reader_thread(LPVOID lpParam) {

    ThreadObject* obj = (ThreadObject*)lpParam;
    HANDLE hReadEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, TEXT("READ_EVENT"));
    HANDLE hWriteEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, TEXT("WRITE_EVENT"));

    std::filesystem::path workdir = std::filesystem::current_path() / "text";

    std::filesystem::directory_iterator begin(workdir);
    std::filesystem::directory_iterator end;

    std::vector<std::filesystem::path> file_list;

    std::copy_if(begin, end, std::back_inserter(file_list), [](std::filesystem::path path) {
        return std::filesystem::is_regular_file(path) && (path.extension() == ".txt");
        });


    for_each(file_list.begin(), file_list.end(), [](std::filesystem::path path){
        std::cout << path.string() << std::endl;
    });

    std::for_each(file_list.begin(), file_list.end(), [&](std::filesystem::path path) {
        WaitForSingleObject(hReadEvent, INFINITE);
        ReaderAsync reader(path.string());
        int byte_received = 0;
        do {
            reader.read();
            //std::cout << "Reader file desc : " << reader.getFileDesc() << std::endl;
            //std::cout << "Reader file offset : " << reader.getFileOffset() << std::endl;
            //std::cout << "Reader number of bytes : " << reader.getNBites() << std::endl;
            //std::cout << "Reader error code : " << reader.getReadState() << std::endl;
            WaitForSingleObject(reader.getFileEvent(), INFINITE);
            ResetEvent(reader.getFileEvent());
            byte_received = reader.returnRes();
            if (byte_received)
                obj->fileinfo.prev_value = obj->fileinfo.byte_received;
            obj->fileinfo.byte_received += byte_received;
            if (byte_received > 0) {
                obj->fileinfo.buffer.append(reader.getBuffer());
            }
        } while (byte_received > 0);
        ResetEvent(hReadEvent);
        SetEvent(hWriteEvent);
        });

    exit_flag = true;

    return 0;
}

DWORD WINAPI writer_thread(LPVOID lpParam) {

    ThreadObject* obj = (ThreadObject*)lpParam;
    HANDLE hReadEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, TEXT("READ_EVENT"));
    HANDLE hWriteEvent = OpenEvent(EVENT_ALL_ACCESS, FALSE, TEXT("WRITE_EVENT"));

    std::filesystem::path workdir = std::filesystem::current_path() / "result.txt";

    WriterAsync writer(workdir.string());


    while (!exit_flag) {
        WaitForSingleObject(hWriteEvent, INFINITE);
        if (!obj->fileinfo.byte_received)
            continue;

        size_t byte_to_handle = obj->fileinfo.byte_received - obj->fileinfo.prev_value;
        std::string temp_buffer;
        temp_buffer.resize(obj->fileinfo.buffer.length());
        std::copy(obj->fileinfo.buffer.begin(), obj->fileinfo.buffer.end(), temp_buffer.begin());
        do {
            size_t chunk_size = (byte_to_handle > CHUNK_SIZE) ? CHUNK_SIZE : byte_to_handle;
            std::cout << "Fileinfo byte received : " << obj->fileinfo.byte_received << " Fileinfo previous value : " << obj->fileinfo.prev_value << std::endl;
            std::cout << "Byte received : " << byte_to_handle << " Chunk size : " << chunk_size << std::endl;
            std::cout << "Buffer contain : " << temp_buffer << std::endl;
            byte_to_handle -= chunk_size;
            std::string::iterator str_itr = temp_buffer.begin();
            str_itr += obj->fileinfo.prev_value;
            std::string::iterator end = str_itr + chunk_size;
            writer.write(str_itr, end);
            WaitForSingleObject(writer.getFileEvent(), INFINITE);
            writer.returnRes();
        } while (byte_to_handle > 0);

        ResetEvent(hWriteEvent);
        SetEvent(hReadEvent);
    }

    return 0;
}

int main() {

    FileInfo fileinfo;
    ZeroMemory(&fileinfo, sizeof(fileinfo));
    fileinfo.buffer = std::string();
    ThreadObject obj;
    ZeroMemory(&obj, sizeof(obj));

    obj.fileinfo = fileinfo;

    HANDLE hReadEvent = CreateEvent(NULL, TRUE, FALSE, TEXT("READ_EVENT"));
    if (hReadEvent == INVALID_HANDLE_VALUE) {
        std::cout << "Cannot create event, error : " << GetLastError() << std::endl;
    }
    HANDLE hWriteEvent = CreateEvent(NULL, TRUE, FALSE, TEXT("WRITE_EVENT"));

    if (hWriteEvent == INVALID_HANDLE_VALUE) {
        std::cout << "Cannot create event, error : " << GetLastError() << std::endl;
    }

    SetEvent(hReadEvent);

    HANDLE thread_read = CreateThread(NULL, 0, reader_thread, &obj, 0, NULL);

    HANDLE thread_write = CreateThread(NULL, 0, writer_thread, &obj, 0, NULL);

    WaitForSingleObject(thread_read, INFINITE);
    CloseHandle(thread_read);
    WaitForSingleObject(thread_write, INFINITE);
    CloseHandle(thread_write);

    CloseHandle(hReadEvent);
    CloseHandle(hWriteEvent);

    //std::cout << fileinfo.buffer << std::endl;   


    //std::for_each(fileinfo.buffer.begin(), fileinfo.buffer.end(), [](auto param){
    //    std::cout << param << std::endl;
    //});


    return 0;
}