#include "writer.h"

    WriterAsync::WriterAsync(std::string file_path) {
        ZeroMemory(&(this->overlapped), sizeof(this->overlapped));
        this->buffer.fill(0);
        this->byte_writed = 0;
        this->bytes_to_write = 0;
        this->hFile = CreateFileA(file_path.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (this->hFile == INVALID_HANDLE_VALUE) {
            std::cout << "Cannot open file, error : " << GetLastError() << std::endl;
        }
        this->hEvent = CreateEventA(NULL, TRUE, FALSE, file_path.c_str());
        if (this->hEvent == INVALID_HANDLE_VALUE) {
            std::cout << "Cannot create event, error : " << GetLastError() << std::endl;
        }
        this->overlapped.hEvent = this->hEvent;
    }

    int WriterAsync::write(std::string::iterator begin, std::string::iterator end) {
        size_t counter = 0;
        for (counter = 0; begin < end; ++begin, ++counter) {
            //std::cout << "Buffer element(writer)before : "<< this->buffer[counter] << std::endl;
            this->buffer[counter] = *begin;
            std::cout << "Buffer element(writer) : " << this->buffer[counter] << std::endl;
            std::cout << "Iterator element : " << (int)*begin << std::endl;
        }
        this->bytes_to_write = counter;

        std::cout << std::endl;


        return WriteFile(this->hFile, this->buffer.data(), this->bytes_to_write, &(this->byte_writed), &(this->overlapped));
    }

    int WriterAsync::getWriteState() {
        return 0;
    }

    int WriterAsync::returnRes() {
        this->overlapped.Offset += this->byte_writed;
        return this->byte_writed;
    }

    WriterAsync::~WriterAsync() {
        CloseHandle(this->hFile);
        CloseHandle(this->hEvent);
    }

    HANDLE WriterAsync::getFileDesc() {
        return this->hFile;
    }

    HANDLE WriterAsync::getFileEvent() {
        return this->hEvent;
    }

    int WriterAsync::getNBites() {
        return this->byte_writed;
    }
