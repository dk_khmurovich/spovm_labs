#ifndef WRITER_ASYNC
#define WRITER_ASYNC

#include <Windows.h>
#include <string>
#include <array>
#include <iostream>
#include <cstring>

#define CHUNK_SIZE 1024

class WriterAsync{
private:
    HANDLE hFile;
    HANDLE hEvent;
    DWORD byte_writed;
    DWORD bytes_to_write;
    OVERLAPPED overlapped;
    std::array<char, CHUNK_SIZE> buffer;
    WriterAsync(){};
public:
    WriterAsync(std::string);
    int write(std::string::iterator, std::string::iterator);
    int getWriteState();
    int returnRes();
    HANDLE getFileDesc();
    HANDLE getFileEvent();
    int getNBites();
    ~WriterAsync();
};


#endif // ! - WRITER_ASYNC