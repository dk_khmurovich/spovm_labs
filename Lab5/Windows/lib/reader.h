#ifndef READER_ASYNC
#define READER_ASYNC

#include <Windows.h>
#include <string>
#include <array>
#include <iostream>
#include <cstring>

#define BUFFER_SIZE 1024

class ReaderAsync{
private:
    HANDLE hFile;
    HANDLE hEvent;
    DWORD byte_readed;
    OVERLAPPED overlapped;
    std::array<char, BUFFER_SIZE> buffer;
    ReaderAsync(){};
public:
    ReaderAsync(std::string);
    int read();
    int getReadState();
    std::string getBuffer();
    int returnRes();
    void emptyBuffer();
    ~ReaderAsync();
    HANDLE getFileDesc();
    HANDLE getFileEvent();
    int getNBites();
};


#endif // ! - READER_ASYNC