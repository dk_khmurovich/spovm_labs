#include "reader.h"

    ReaderAsync::ReaderAsync(std::string file_path) {
        ZeroMemory(&(this->overlapped), sizeof(this->overlapped));
        this->byte_readed = 0;
        this->hFile = CreateFileA(file_path.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (this->hFile == INVALID_HANDLE_VALUE) {
            std::cout << "Cannot open file, error : " << GetLastError() << std::endl;
        }
        this->hEvent = CreateEventA(NULL, TRUE, FALSE, file_path.c_str());
        if (this->hEvent == INVALID_HANDLE_VALUE) {
            std::cout << "Cannot create event, error : " << GetLastError() << std::endl;
        }
        this->overlapped.hEvent = this->hEvent;
    }

    int ReaderAsync::read() {
        return ReadFile(this->hFile, this->buffer.data(), BUFFER_SIZE, &(this->byte_readed), &(this->overlapped));
    }

    int ReaderAsync::getReadState() {
        return 0;
    }

    std::string ReaderAsync::getBuffer() {
        std::string temp_res;
        temp_res.resize(this->byte_readed);
        std::copy(this->buffer.begin(), this->buffer.begin()+this->byte_readed, temp_res.begin());
        std::fill_n(this->buffer.begin(), BUFFER_SIZE, 0);
        return temp_res;
    }

    int ReaderAsync::returnRes() {
        this->overlapped.Offset += this->byte_readed;
        return this->byte_readed;
    }

    void ReaderAsync::emptyBuffer() {
        for (size_t counter = 0; counter < BUFFER_SIZE; ++counter)
            this->buffer[counter] = 0;
    }

    ReaderAsync::~ReaderAsync() {
        CloseHandle(this->hFile);
        CloseHandle(this->hEvent);
    }

    HANDLE ReaderAsync::getFileDesc() {
        return this->hFile;
    }

    HANDLE ReaderAsync::getFileEvent() {
        return this->hEvent;
    }

    int ReaderAsync::getNBites() {
        return this->byte_readed;
    }
