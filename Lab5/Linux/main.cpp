/*-----------------------------------------------------------------------------------------------*
*g++ -Wall -Werror -fpic -shared -lpthread -lrt -std=c++17 reader.cpp writer.cpp -o libioasync.so*
*LD_LIBRARY_PATH=.                                                                               *
*export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH                                                       *
*g++ main.cpp -o app -lioasync -lrt -lpthread -std=c++17 -L.                                     *
*------------------------------------------------------------------------------------------------*/

#include <filesystem>
#include <algorithm>
#include "reader.h"
#include "writer.h"


typedef struct {
    std::string buffer;
    size_t byte_received;
    size_t prev_value;
}FileInfo;

bool exit_flag = false;

FileInfo fileinfo;

pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;

void* reader_thread(void* param){
    std::filesystem::path workdir = std::filesystem::current_path() / "text";

    std::filesystem::recursive_directory_iterator begin(workdir);
    std::filesystem::recursive_directory_iterator end;

    std::vector<std::filesystem::path> file_list;

    std::copy_if(begin, end, std::back_inserter(file_list), [](std::filesystem::path path){
        return std::filesystem::is_regular_file(path) && (path.extension() == ".txt");
    });


    //for_each(file_list.begin(), file_list.end(), [](std::filesystem::path path){
    //    std::cout << path.native() << std::endl;
    //});

    std::for_each(file_list.begin(), file_list.end(), [](std::filesystem::path path){
        pthread_mutex_lock(&read_lock);
        //std::cout << "I'm stuck in reading" << std::endl; 
        ReaderAsync reader(path.native());
        int byte_received = 0;
        do{
            reader.read();
            //std::cout << "Reader file desc : " << reader.getFileDesc() << std::endl;
            //std::cout << "Reader file offset : " << reader.getFileOffset() << std::endl;
            //std::cout << "Reader number of bytes : " << reader.getNBites() << std::endl;
            //std::cout << "Reader error code : " << reader.getReadState() << std::endl;
            while(reader.getReadState() == EINPROGRESS){};
            byte_received = reader.returnRes();
            if(byte_received)
                fileinfo.prev_value = fileinfo.byte_received;
            fileinfo.byte_received += byte_received;
            if(byte_received > 0){
                fileinfo.buffer += reader.getBuffer();
            }
        }while(byte_received > 0);
        pthread_mutex_unlock(&write_lock);
    });

    exit_flag = true;

    pthread_exit(0);
}

void* writer_thread(void* param){

    std::filesystem::path workdir = std::filesystem::current_path() / "result.txt";

    WriterAsync writer(workdir.native());


    while(!exit_flag){
        pthread_mutex_lock(&write_lock);
        //std::cout << "I'm stuck in writing" << std::endl; 
        if(!fileinfo.byte_received)
            continue;

        size_t byte_to_handle = fileinfo.byte_received - fileinfo.prev_value;
        do{
            size_t chunk_size = (byte_to_handle > CHUNK_SIZE) ? CHUNK_SIZE : byte_to_handle;
            //std::cout << "Byte received : " << byte_to_handle << " Chunk size : " << chunk_size << std::endl;
            //std::cout << "Buffer contain : " << fileinfo.buffer << std::endl;
            byte_to_handle -= chunk_size;
            std::string::iterator str_itr = fileinfo.buffer.begin() + fileinfo.prev_value;
            if(*str_itr == 127){
                ++str_itr;
                ++fileinfo.byte_received;
                ++fileinfo.prev_value;
            }
            writer.write(str_itr, str_itr+chunk_size);
            while(writer.getWriteState() == EINPROGRESS){};
            writer.returnRes();
        }while(byte_to_handle > 0);
        pthread_mutex_unlock(&read_lock);
    }

    pthread_exit(0);
}

int main(){
    pthread_t read_thread;
    pthread_t write_thread;
    pthread_attr_t attr;

    fileinfo.byte_received = 0;
    fileinfo.prev_value = 0;
    fileinfo.buffer = std::string();

    pthread_attr_init(&attr);

    pthread_create(&read_thread, &attr, reader_thread, NULL);
    pthread_mutex_trylock(&write_lock);
    pthread_create(&write_thread, &attr, writer_thread, NULL);
    pthread_join(read_thread, NULL);
    pthread_join(write_thread, NULL);


    //std::for_each(fileinfo.buffer.begin(), fileinfo.buffer.end(), [](auto param){
    //    std::cout << param << std::endl;
    //});


    return 0;
}