#ifndef WRITER_ASYNC
#define WRITER_ASYNC

#include <aio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <array>
#include <iostream>
#include <cstring>

#define CHUNK_SIZE 1024

class WriterAsync{
private:
    int filedesc;
    int byte_transferred;
    struct aiocb aiocb;
    std::array<char, CHUNK_SIZE> buffer;
    WriterAsync(){};
public:
    WriterAsync(std::string);
    int write(std::string::iterator, std::string::iterator);
    int getWriteState();
    int returnRes();
    int getFileDesc();
    int getNBites();
    ~WriterAsync();
};


#endif // ! - WRITER_ASYNC