#ifndef READER_ASYNC
#define READER_ASYNC

#include <aio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <array>
#include <iostream>
#include <cstring>

#define BUFFER_SIZE 1024

class ReaderAsync{
private:
    int filedesc;
    int byte_received;
    struct aiocb aiocb;
    std::array<char, BUFFER_SIZE> buffer;
    ReaderAsync(){};
public:
    ReaderAsync(std::string);
    int read();
    int getReadState();
    std::string getBuffer();
    int returnRes();
    void emptyBuffer();
    ~ReaderAsync();
    int getFileDesc();
    int getNBites();
};


#endif // ! - READER_ASYNC