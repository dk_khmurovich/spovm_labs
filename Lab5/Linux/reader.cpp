#include "reader.h"

    ReaderAsync::ReaderAsync(std::string file_path) {
        memset(&(this->aiocb), 0, sizeof(this->aiocb));
        this->filedesc = open(file_path.c_str(), O_RDONLY);
        this->aiocb.aio_fildes = this->filedesc;
        this->aiocb.aio_buf = this->buffer.data();
        this->aiocb.aio_offset = 0;
        this->aiocb.aio_nbytes = BUFFER_SIZE;
        this->aiocb.aio_reqprio = 0;
        this->aiocb.aio_lio_opcode = 0;
        this->aiocb.aio_sigevent.sigev_notify = SIGEV_NONE;
        this->byte_received = 0;
    }

    int ReaderAsync::read() {
        return aio_read(&(this->aiocb));
    }

    int ReaderAsync::getReadState() {
        return aio_error(&(this->aiocb));
    }

    std::string ReaderAsync::getBuffer() {
        std::string temp_res = std::string(this->buffer.data());
        std::fill_n(this->buffer.begin(), BUFFER_SIZE, 0);
        return temp_res;
    }

    int ReaderAsync::returnRes() {
        this->byte_received = aio_return(&(this->aiocb));;
        this->aiocb.aio_offset += this->byte_received;
        return this->byte_received;
    }

    void ReaderAsync::emptyBuffer() {
        for (size_t counter = 0; counter < BUFFER_SIZE; ++counter)
            this->buffer[counter] = 0;
    }

    ReaderAsync::~ReaderAsync() {
       close(this->filedesc);
    }

    int ReaderAsync::getFileDesc() {
        return this->filedesc;
    }


    int ReaderAsync::getNBites() {
        return this->byte_received;
    }
