#include "writer.h"

    WriterAsync::WriterAsync(std::string file_path) {
        memset(&(this->aiocb), 0, sizeof(this->aiocb));
        this->filedesc = open(file_path.c_str(), O_WRONLY);
        this->aiocb.aio_fildes = this->filedesc;
        this->aiocb.aio_buf = this->buffer.data();
        this->aiocb.aio_offset = 0;
        this->aiocb.aio_nbytes = 0;
        this->aiocb.aio_reqprio = 0;
        this->aiocb.aio_lio_opcode = 0;
        this->aiocb.aio_sigevent.sigev_notify = SIGEV_NONE;
        this->byte_transferred = 0;
    }

    int WriterAsync::write(std::string::iterator begin, std::string::iterator end) {
        size_t counter = 0;
        for (counter = 0; begin < end; ++begin, ++counter) {
            //std::cout << "Buffer element(writer)before : "<< this->buffer[counter] << std::endl;
            this->buffer[counter] = *begin;
            //std::cout << "Buffer element(writer) : " << this->buffer[counter] << std::endl;
            //std::cout << "Iterator element : " << (int)*begin << std::endl;
        }
        this->byte_transferred = counter;
        this->aiocb.aio_nbytes = this->byte_transferred;


        return aio_write(&(this->aiocb));
    }

    int WriterAsync::getWriteState() {
        return aio_error(&(this->aiocb));
    }

    int WriterAsync::returnRes() {
        this->byte_transferred = aio_return(&(this->aiocb));
        this->aiocb.aio_offset += this->byte_transferred;
        return this->byte_transferred;
    }

    WriterAsync::~WriterAsync() {
        close(this->filedesc);
    }

    int WriterAsync::getFileDesc() {
        return this->filedesc;
    }

    int WriterAsync::getNBites() {
        return this->byte_transferred;
    }
