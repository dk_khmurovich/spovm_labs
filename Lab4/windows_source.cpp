#include <iostream>
#include <Windows.h>
#include <conio.h>
#include <vector>



DWORD WINAPI thread_function(LPVOID lpParam){
    //std::cout << "Thread created!" << std::endl;
    int pid = 0;
    pid = GetCurrentThreadId();
    char string_to_out[30] = {0};
    sprintf(string_to_out, "hello%d", pid);
    char eventName[30] = {0};
    sprintf(eventName, "ChildEventStart%d", pid);
    HANDLE hEventStart = OpenEventA(EVENT_ALL_ACCESS, TRUE, eventName);
    sprintf(eventName, "ChildEventEnd%d", pid);
    HANDLE hEventEnd = OpenEventA(EVENT_ALL_ACCESS, TRUE, "END_WRITE");
    HANDLE eventArr[] = {hEventStart, hEventEnd};
    while(1){
      if(WaitForMultipleObjects(2, eventArr, FALSE, INFINITY) == WAIT_OBJECT_0){
        ResetEvent(hEventStart);
        int counter = 0;
        while(string_to_out[counter]){
            Sleep(250);
            printf("%c", string_to_out[counter]);
            ++counter;
        }
        printf("%d\n", pid);
        SetEvent(hEventEnd);
      }
    }
    return 0;
}


HANDLE createThread(){
    HANDLE threadid = CreateThread(NULL, 0, thread_function, NULL, 0, NULL);
    if(threadid){
        return threadid;
    }
    printf("Process not created\n");
    return 0;
}

int getchBundle()
{
	if (_kbhit()) return _getch();
	else return -1;
}

int main(int argc, char* argv[]){
    HANDLE pids[101];
    HANDLE hResourceEventsStart[101];
    HANDLE hResourceEventEnd = CreateEventA(NULL, TRUE, TRUE, "END_WRITE");
    int pidNumber = 0;
    int currentProccess = pidNumber;
    int prevProcess = 0;
    int new_proc_flag = FALSE;
    char symbol = 0;
    int end = 0;
    char eventName[30] = {0};

    while(!end){
        symbol = getchBundle();
        //std::cout << "Loop test" << std::endl;
        switch (symbol){
        case '+':
            if(pidNumber > 101)
                break;
            pids[pidNumber] = createThread();
            sprintf(eventName, "ChildEventStart%d", GetThreadId(pids[pidNumber]));
            hResourceEventsStart[pidNumber] = CreateEventA(NULL, TRUE, FALSE, eventName);
            if(pidNumber == 0)
                SetEvent(hResourceEventsStart[pidNumber]);
            ++pidNumber;
            break;
        case '-':
            if(!pidNumber)
                break;
            if(WaitForSingleObject(hResourceEventEnd, INFINITY) == WAIT_OBJECT_0){
                TerminateThread(pids[pidNumber - 1], 0);
                CloseHandle(hResourceEventsStart[pidNumber - 1]);
                if(currentProccess == pidNumber - 1)
                    currentProccess = 0;
            }
            --pidNumber;
            break;
        case 'q':
            while (pidNumber){
			    if(WaitForSingleObject(hResourceEventEnd, INFINITY) == WAIT_OBJECT_0){
                TerminateThread(pids[pidNumber - 1], 0);
                CloseHandle(hResourceEventsStart[pidNumber - 1]);
                --pidNumber;
		        }
            }
            CloseHandle(hResourceEventEnd);
            return 0;
        default:
            break;
        }
        if(pidNumber){
            prevProcess = (currentProccess) ? (currentProccess-1) : (pidNumber - 1); 
            if(WaitForSingleObject(hResourceEventEnd, INFINITY) == WAIT_OBJECT_0){
                SetEvent(hResourceEventsStart[currentProccess]);
                ResetEvent(hResourceEventEnd);
                ++currentProccess;
                if(currentProccess >= pidNumber){
                    currentProccess = 0;
                }
            }                
        }
    }
    //CloseHandle(hResourceEvent);
    return 0;
}