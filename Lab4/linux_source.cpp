#include <signal.h>
#include <termios.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stropts.h>
#include <string>
#include <iostream>
#include <time.h>
#include <pthread.h>

typedef struct{
    pthread_mutex_t mutex;
    bool exit;
}Mutex;

Mutex mutexes[101];
int end_write = 0;
pthread_mutex_t mutex_end_write;

int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;
 
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
 
  ch = getchar();
 
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);
 
  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }
 
  return 0;
}

int getch()
{
  struct termios oldt, newt;
  int ch;
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  return ch;
}

int getchBundle()
{
	if (kbhit()) return getch();
	else return -1;
}

void* thread_function(void* param){
    //std::cout << "Thread created!" << std::endl;
    int pid = 0;
    pid = pthread_self();
    char string_to_out[]{"hello"};
    Mutex* mutex = (Mutex*)param;
    while(!mutex->exit){
        pthread_mutex_lock(&(mutex->mutex));
        if(mutex->exit)
            pthread_exit(0);
        printf("%d", pid);
        fflush(stdout);
        int counter = 0;
        while(string_to_out[counter]){
            usleep(100000);
            printf("%c", string_to_out[counter]);
            fflush(stdout);
            ++counter;
        }
        std::cout << std::endl;
        end_write = 1;
        pthread_mutex_unlock(&mutex_end_write);
        //while(pthread_mutex_trylock(&mutex)){}
    }
    
    pthread_exit(0);
}

int main(int argc, char* argv[]){
    pthread_t pids[101];
    pthread_attr_t attr;
    int pidNumber = 0;
    int end = 0;
    int currentThread = 0;

    pthread_attr_init(&attr);
    pthread_mutex_init(&mutex_end_write, NULL);

    while(!end){
        pthread_mutex_lock(&mutex_end_write);
        //std::cout << "main loop" << std::endl;
        switch(getchBundle()){
            case '+':
                if(pidNumber > 101)
                    break;
                pthread_mutex_init(&(mutexes[pidNumber].mutex), NULL);
                mutexes[pidNumber].exit = false;
                if(pidNumber) pthread_mutex_trylock(&(mutexes[pidNumber]).mutex);
                pthread_create(&pids[pidNumber], &attr, thread_function, &mutexes[pidNumber]);
                ++pidNumber;
            break;
            case '-':
                if(!pidNumber)
                    break;
                mutexes[pidNumber - 1].exit = true;
                pthread_mutex_unlock(&(mutexes[pidNumber - 1]).mutex);
                pthread_join(pids[pidNumber - 1], NULL);
                pthread_mutex_destroy(&(mutexes[pidNumber - 1]).mutex);
                //pthread_cancel(pids[pidNumber - 1]);
                --pidNumber;
            break;
            case 'q':
                end = 1;
            break;
            default: break;
        }
        if(pidNumber){
                if(end_write){
                    //std::cout << "Writing ended" << std::endl;
                    ++currentThread;
                    if(currentThread >= pidNumber){
                        currentThread = 0;
                    }
                    pthread_mutex_unlock(&(mutexes[currentThread].mutex));
                    end_write = 0;
                }
            }
        else{
            pthread_mutex_unlock(&mutex_end_write);
        }
    }
    mutexes[pidNumber - 1].exit = true;
    pthread_join(pids[pidNumber - 1], NULL);
    --pidNumber;
    while(pidNumber){
        pthread_cancel(pids[pidNumber - 1]);
        pthread_mutex_destroy(&(mutexes[pidNumber - 1]).mutex);
        --pidNumber;
    }

    
    return 0;
}