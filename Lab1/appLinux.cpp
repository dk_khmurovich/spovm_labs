#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <ncurses.h>
#include <time.h>

WINDOW* CreateNewWindow(int height, int width, int startx, int starty){
    WINDOW* new_window;
    new_window = newwin(height, width, startx, starty);
    refresh();
    //box(new_window, 0, 0);
    return new_window;
}

int main(){
    initscr();
    cbreak();
    int pid = fork();
    time_t cur_time = time(NULL);
    WINDOW* window;
    switch(pid){
        case -1:
            printw("Error\n");
            refresh();
            return -1;
        case 0:
            window = CreateNewWindow(8, 30, 2, 53);
            wprintw(window,"%schild thread\n", ctime(&cur_time));
            wrefresh(window);
            getch();
            delwin(window);
            return 1;
        default:
            window = CreateNewWindow(8, 30, 2, 10);
            wprintw(window, "%sparent thread\n", ctime(&cur_time));
            wrefresh(window);
            getch();
            delwin(window);
            break;
    }
    int status;
    waitpid(-1, &status,0);
    endwin();
    return 0;
}