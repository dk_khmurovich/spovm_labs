#include <iostream>
#include <Windows.h>

int main(int argc, char* argv[]){
    STARTUPINFO info;
    ZeroMemory(&info, sizeof(STARTUPINFO));
    info.cb = sizeof(info);
    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));

    if(!CreateProcess(NULL, argv[1], NULL, NULL, FALSE, 0, NULL, NULL, &info, &pi)){
        return 0;
    }
    std::cout << "This is a parent process?" << std::endl;
    WaitForSingleObject(pi.hProcess, INFINITE);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    return 0;
}